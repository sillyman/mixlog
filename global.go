package mixlog

import (
	"os"
)

// global 全局的记录器
var global = &MixLog{RuntimeCallerSkip: 4}

// SetGlobalHandlers 设置全部的处理器
func SetGlobalHandlers(handlers ...*handler) { global.SetHandler(handlers...) }

// SetGlobalToSimple 将全局对象设置为简单模式
func SetGlobalToSimple() {
	global.SetHandler(NewHandlerToWriter(LvlInfo, MustNewFormatter("[{{.Lvl}}] {{.Time}} {{.Message}}", "2006-01-02 15:04:05"), os.Stdout, false))
}

// SetGlobalToGeneric 将全局对象设置为通用模式
func SetGlobalToGeneric(stdLvl, fileLvl Lvl) {
	global.SetHandler(
		NewHandlerToWriter(stdLvl, MustNewFormatter("[{{.Lvl}}] {{.Time}} {{.Message}}", "2006-01-02 15:04:05"), os.Stdout, false),
		MustNewHandlerToFile(
			fileLvl, MustNewFormatter(FormatFuncNameShortFileName, "2006/01/02T15:04:05.000-07:00"),
			"main.log", false,
		),
	)
}

// init 为全局记录器设置 handlers，默认情况下日志写入到 os.Stdout
func init() { SetGlobalToSimple() }

func Debug(a ...interface{})   { global.Debug(a...) }
func Info(a ...interface{})    { global.Info(a...) }
func Warning(a ...interface{}) { global.Warning(a...) }
func Error(a ...interface{})   { global.Error(a...) }
func Fatal(a ...interface{})   { global.Fatal(a...) }

func Debugf(format string, a ...interface{})   { global.Debugf(format, a...) }
func Infof(format string, a ...interface{})    { global.Infof(format, a...) }
func Warningf(format string, a ...interface{}) { global.Warningf(format, a...) }
func Errorf(format string, a ...interface{})   { global.Errorf(format, a...) }
func Fatalf(format string, a ...interface{})   { global.Fatalf(format, a...) }
