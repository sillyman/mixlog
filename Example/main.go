package main

import (
	"os"

	"gitee.com/sillyman/mixlog"
)

func main() {
	// 直接使用全局的日志记录器，默认的等级是 INFO
	mixlog.Debug("我是一条 DEBUG 信息")
	mixlog.Info("我是一条 INFO 信息")
	mixlog.Warning("我是一条 WARNING 信息")
	mixlog.Error("我是一条 ERROR 信息")

	// 重新设置全局日志记录器的 Handler
	mixlog.SetGlobalHandlers(
		mixlog.NewHandlerToWriter(
			mixlog.LvlDebug, mixlog.MustNewFormatter(mixlog.FormatFuncNameLongFileName, ""),
			os.Stdout, false,
		),
	)
	mixlog.Debug("我是一条 DEBUG 信息")
	mixlog.Info("我是一条 INFO 信息")
	mixlog.Warning("我是一条 WARNING 信息")
	mixlog.Error("我是一条 ERROR 信息")

	// 创建一个日志对象，包括了2个handler
	log := mixlog.NewMixLog(
		mixlog.NewHandlerToWriter(
			mixlog.LvlDebug, mixlog.MustNewFormatter(mixlog.FormatFuncNameLongFileName, "20060102 15:04:05-07:00"),
			os.Stdout, false,
		),
		mixlog.MustNewHandlerToFile(
			mixlog.LvlDebug, mixlog.MustNewFormatter(mixlog.FormatFuncNameShortFileName, "2006/01/02T15:04:05.000-07:00"),
			"test.log", true,
		),
	)
	log.Debug("我是一条 DEBUG 信息")
	log.Info("我是一条 INFO 信息")
	log.Warning("我是一条 WARNING 信息")
	log.Error("我是一条 ERROR 信息")
	log.Fatal("我是一条 FATAL 信息，程序会退出 `os.Exit(1)`")
}
