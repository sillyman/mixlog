# 日志记录器

## 介绍

支持颜色输出。支持各handler分别定义formatter和等级。

使用的第三方包

- github.com/fatih/color 用于输出颜色log
- github.com/mattn/go-colorable 用于在Windows中的颜色log
- github.com/lestrrat-go/file-rotatelogs 用于滚动记录日志文件




#### 安装教程

```base
go get gitee.com/sillyman/mixlog
```

## 使用说明

请参考示例。

### 怎么定义输出格式？

在创建Handler的时候，需要提供一个 *formatter 对象，由 formatter 定义日志样式和日志的时间格式。在模块已经提供了4个常用的日志样式模板。

    FormatLongFileName          = "{{.Lvl}} - {{.Time}} - {{.LongFileName}}({{.Line}}) - {{.Message}}"
    FormatShortFileName         = "{{.Lvl}} - {{.Time}} - {{.ShortFilename}}({{.Line}}) - {{.Message}}"
    FormatFuncNameLongFileName  = "{{.Lvl}} - {{.Time}} - {{.FuncName}} - {{.LongFileName}}({{.Line}}) - {{.Message}}"
    FormatFuncNameShortFileName = "{{.Lvl}} - {{.Time}} - {{.FuncName}} - {{.ShortFilename}}({{.Line}}) - {{.Message}}"

日志样式提供了7个模板变量(**区分大小写**)，分别是：

- `{{.Lvl}}` 日志等级的字符串形式，如INFO
- `{{.Time}}` 时间，时间的格式由 `formatter.TimeLayout` 定义
- `{{.LongFileName}}` 调用位置的完整文件名
- `{{.ShortFilename}}` 调用位置的基本文件名
- `{{.Line}}` 调用位置所在文件的行号
- `{{.FuncName}}` 调用位置所在函数名称
- `{{.Message}}` 消息内容

### 怎么重新定义颜色？

使用 `github.com/fatih/color` 输出颜色，支持Linux和Windows。模块的一个全局变量 `LvlColorMap` 定义等级的输出颜色，重新定义它就可以修改颜色了。

```go
var LvlColorMap = map[Lvl]*color.Color{
	LvlDebug:   color.New(color.FgWhite),
	LvlInfo:    color.New(color.FgCyan),
	LvlWarning: color.New(color.FgYellow),
	LvlError:   color.New(color.FgHiRed),
	LvlFatal:   color.New(color.FgMagenta),
}
```

### 使用示例

```go
package main

import (
	"os"

	"gitee.com/sillyman/mixlog"
)

func main() {
	// 直接使用全局的日志记录器，默认的等级是 INFO
	mixlog.Debug("我是一条 DEBUG 信息")
	mixlog.Info("我是一条 INFO 信息")
	mixlog.Warning("我是一条 WARNING 信息")
	mixlog.Error("我是一条 ERROR 信息")

	// 重新设置全局日志记录器的 Handler
	mixlog.SetGlobalHandlers(
		mixlog.NewHandlerToWriter(
			mixlog.LvlDebug, mixlog.MustNewFormatter(mixlog.FormatFuncNameLongFileName, ""),
			os.Stdout, false,
		),
	)
	mixlog.Debug("我是一条 DEBUG 信息")
	mixlog.Info("我是一条 INFO 信息")
	mixlog.Warning("我是一条 WARNING 信息")
	mixlog.Error("我是一条 ERROR 信息")

	// 创建一个日志对象，包括了2个handler
	log := mixlog.NewMixLog(
		mixlog.NewHandlerToWriter(
			mixlog.LvlDebug, mixlog.MustNewFormatter(mixlog.FormatFuncNameLongFileName, "20060102 15:04:05-07:00"),
			os.Stdout, false,
		),
		mixlog.MustNewHandlerToFile(
			mixlog.LvlDebug, mixlog.MustNewFormatter(mixlog.FormatFuncNameShortFileName, "2006/01/02T15:04:05.000-07:00"),
			"test.log", true,
		),
	)
	log.Debug("我是一条 DEBUG 信息")
	log.Info("我是一条 INFO 信息")
	log.Warning("我是一条 WARNING 信息")
	log.Error("我是一条 ERROR 信息")
	log.Fatal("我是一条 FATAL 信息，程序会退出 `os.Exit(1)`")
}
```

以上创建了一个 `test.log` 文件，并且控制台打印如下：

![截图](./Example/screenshot.jpg)