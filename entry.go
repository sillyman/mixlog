package mixlog

import (
	"fmt"
	"path/filepath"
	"runtime"
	"time"
)

// entry 是一条日志条目的元素
type entry struct {
	// 模板的字段
	Time          string
	Lvl           string
	Message       string
	FuncName      string
	LongFileName  string
	ShortFilename string
	Line          int

	t     time.Time // 日志产生的时间
	lvlNo Lvl       // 日志等级
}

// newEntry 创建一条日志条目
// 当 runtimeCallerSkip 大于 1 才会获取使用 `runtime.Caller` 获取调用的函数和文件信息
func newEntry(lvl Lvl, msg string, disableCaller bool, runtimeCallerSkip int) entry {
	log := entry{t: time.Now(), lvlNo: lvl, Lvl: fmt.Sprintf("%5s", lvlStringMap[lvl]), Message: msg}

	if !disableCaller {
		if pc, file, line, ok := runtime.Caller(runtimeCallerSkip); ok {
			log.FuncName = runtime.FuncForPC(pc).Name()
			log.Line = line
			log.LongFileName = file
			log.ShortFilename = filepath.Base(log.LongFileName)
		}
	}

	return log
}
