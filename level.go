package mixlog

import (
	"strings"
)

// Lvl 日志等级
type Lvl uint8

const (
	LvlDebug Lvl = iota
	LvlInfo
	LvlWarning
	LvlError
	LvlFatal
)

// lvlStringMap 等级和字符串的映射
var lvlStringMap = map[Lvl]string{
	LvlDebug:   "DEBUG",
	LvlInfo:    "INFO",
	LvlWarning: "WARN",
	LvlError:   "ERROR",
	LvlFatal:   "FATAL",
}

// String 转换成字符串
func (lvl Lvl) String() string {
	return lvlStringMap[lvl]
}

// ParseLevel 解析字符串为日志等级
func ParseLevel(s string) Lvl {
	switch strings.ToLower(s) {
	case "debug":
		return LvlDebug
	case "info":
		return LvlInfo
	case "warning", "warn":
		return LvlWarning
	case "error", "err":
		return LvlError
	case "fatal":
		return LvlFatal
	default:
		return LvlInfo
	}
}
