package mixlog

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseLevel(t *testing.T) {
	tests := []struct {
		name  string
		level Lvl
	}{
		{"DEBUG", LvlDebug},
		{"INFO", LvlInfo},
		{"warn", LvlWarning},
		{"WARNING", LvlWarning},
		{"err", LvlError},
		{"ERROR", LvlError},
		{"FATAL", LvlFatal},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, ParseLevel(tt.name), tt.level)
		})
	}
}
