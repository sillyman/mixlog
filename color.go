package mixlog

import (
	"github.com/fatih/color"
)

// LvlColorMap 等级和颜色的映射
var LvlColorMap = map[Lvl]*color.Color{
	LvlDebug:   color.New(color.FgWhite),
	LvlInfo:    color.New(color.FgCyan),
	LvlWarning: color.New(color.FgYellow),
	LvlError:   color.New(color.FgHiRed),
	LvlFatal:   color.New(color.FgMagenta),
}
